module gitlab.com/etke.cc/mrs/api

go 1.21.0

toolchain go1.21.3

require (
	github.com/VictoriaMetrics/metrics v1.32.0
	github.com/archdx/zerolog-sentry v1.8.2
	github.com/benjaminestes/robots/v2 v2.0.5
	github.com/blevesearch/bleve/v2 v2.3.10
	github.com/cespare/xxhash/v2 v2.2.0
	github.com/fsnotify/fsnotify v1.7.0
	github.com/getsentry/sentry-go v0.27.0
	github.com/goccy/go-json v0.10.2
	github.com/hashicorp/golang-lru/v2 v2.0.7
	github.com/labstack/echo/v4 v4.11.4
	github.com/matrix-org/gomatrixserverlib v0.0.0-20240116122202-14ee7615d604
	github.com/mattevans/postmark-go v1.0.0
	github.com/mileusna/crontab v1.2.0
	github.com/pemistahl/lingua-go v1.4.0
	github.com/raja/argon2pw v1.0.1
	github.com/rs/zerolog v1.32.0
	github.com/theovassiliou/base64url v0.0.0-20211006203958-1e011490eaaf
	github.com/xxjwxc/gowp v0.0.0-20230612082025-23a9b62c1da6
	github.com/ziflex/lecho/v3 v3.5.0
	gitlab.com/etke.cc/go/echo-basic-auth v1.1.0
	gitlab.com/etke.cc/go/fswatcher v1.0.0
	gitlab.com/etke.cc/go/msc1929 v1.0.1
	go.etcd.io/bbolt v1.3.8
	golang.org/x/exp v0.0.0-20240213143201-ec583247a57a
	golang.org/x/time v0.5.0
	gopkg.in/yaml.v3 v3.0.1
	maunium.net/go/mautrix v0.17.0
	mvdan.cc/xurls/v2 v2.5.0
)

require (
	github.com/RoaringBitmap/roaring v1.9.0 // indirect
	github.com/benjaminestes/robots v1.0.0 // indirect
	github.com/bits-and-blooms/bitset v1.13.0 // indirect
	github.com/blevesearch/bleve_index_api v1.1.6 // indirect
	github.com/blevesearch/geo v0.1.20 // indirect
	github.com/blevesearch/go-porterstemmer v1.0.3 // indirect
	github.com/blevesearch/gtreap v0.1.1 // indirect
	github.com/blevesearch/mmap-go v1.0.4 // indirect
	github.com/blevesearch/scorch_segment_api/v2 v2.2.8 // indirect
	github.com/blevesearch/segment v0.9.1 // indirect
	github.com/blevesearch/snowballstem v0.9.0 // indirect
	github.com/blevesearch/stempel v0.2.0 // indirect
	github.com/blevesearch/upsidedown_store_api v1.0.2 // indirect
	github.com/blevesearch/vellum v1.0.10 // indirect
	github.com/blevesearch/zapx/v11 v11.3.10 // indirect
	github.com/blevesearch/zapx/v12 v12.3.10 // indirect
	github.com/blevesearch/zapx/v13 v13.3.10 // indirect
	github.com/blevesearch/zapx/v14 v14.3.10 // indirect
	github.com/blevesearch/zapx/v15 v15.3.13 // indirect
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang/geo v0.0.0-20230421003525-6adc56603217 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/labstack/gommon v0.4.2 // indirect
	github.com/matrix-org/util v0.0.0-20221111132719-399730281e66 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/mschoch/smat v0.2.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	github.com/tidwall/gjson v1.17.1 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/tidwall/sjson v1.2.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fastrand v1.1.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	github.com/valyala/histogram v1.2.0 // indirect
	github.com/xxjwxc/public v0.0.0-20240202084818-49b8bdbdbfa0 // indirect
	github.com/yuin/goldmark v1.7.0 // indirect
	go.mau.fi/util v0.4.0 // indirect
	golang.org/x/crypto v0.19.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/protobuf v1.32.0 // indirect
	gopkg.in/eapache/queue.v1 v1.1.0 // indirect
)
